# README

## Get MacOS Info
To ensure that the script is configured for your architecture run:

``` sh
git clone https://gitlab.com/wuphysics87/whisper && bash "whisper/macos-info.bash"
```

This will output your system info to your Downloads directory. The output will begin with '00' so it is easy to find if you sort alphabetically.


