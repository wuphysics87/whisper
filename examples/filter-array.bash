#!/usr/bin/env bash

HEADING_1="HEADING 1"
HEADING_2="HEADING 2"

clear
MATCH="C"
INT=0
while read -r INPUT; do
    if [ "$MATCH" == "$INPUT" ]; then
        echo "$HEADING_2"
        SOME_QUEUED=true

    elif [ -z "$SOME_QUEUED" ] && [ -z "$SOME_RUNNING" ]; then
        echo "$HEADING_1"
        SOME_RUNNING=true
    fi
    echo "$INT $INPUT"
    INT="$(( INT + 1 ))"
done <<< "$RUN_LIST"
