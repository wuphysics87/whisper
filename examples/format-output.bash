#!/usr/bin/env bash


function format_output(){
    local -n arr=$1
    echo "${arr[@]}" | sed -E "s/[[:space:]]+/\n/g" | column
}

echo "MULTI LINE ARRAY"
readarray -t MULTI_LINE_ARRAY < multi-line-array.txt

format_output MULTI_LINE_ARRAY

echo "SINGLE LINE ARRAY"
read -a SINGLE_LINE_ARRAY < single-line-array.txt

format_output SINGLE_LINE_ARRAY
