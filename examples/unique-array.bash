#!/usr/bin/env bash

ARRAY1=( a b c d e )
ARRAY2=( c d e f g )

readarray -t UNIQUE_ARRAY < <(( printf '%s\n' "${ARRAY1[@]}" "${ARRAY2[@]}" | sort | uniq -u ))


printf "ARRAY1"
printf "\n%s\n\n" "${ARRAY1[*]}"

printf "ARRAY2"
printf "\n%s\n\n" "${ARRAY2[*]}"

printf "UNIQUE"
printf "\n%s\n\n" "${UNIQUE_ARRAY[*]}"

for INDEX in "${!ARRAY1[@]}"; do
    if [[ " ${ARRAY2[*]} " != *" ${ARRAY1[$INDEX]} "* ]]; then
        ONLY_ARRAY1+=( "${ARRAY1[$INDEX]}" )
    fi
done


printf "ONLY IN ARRAY 1"
printf "\n%s\n\n" "${ONLY_ARRAY1[*]}"

for INDEX in "${!ARRAY2[@]}"; do
    if [[ " ${ARRAY1[*]} " != *" ${ARRAY2[$INDEX]} "* ]]; then
        ONLY_ARRAY2+=( "${ARRAY2[$INDEX]}" )
    fi
done


printf "ONLY IN ARRAY 2"
printf "\n%s\n\n" "${ONLY_ARRAY2[*]}"
