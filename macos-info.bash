#!/usr/bin/env bash

function get_python_info () {

  printf "\n\n ===== Python Version =====\n"
    python -V

  printf "\n\n ===== Python Path =====\n"
    which python

  printf "\n\n ===== Pip Version =====\n"
    pip -V

  printf "\n\n ===== Pip Path =====\n"
    which pip
}

function get_python3_info () {

  printf "\n\n ===== Python3 Version =====\n"
    python3 -V

  printf "\n\n ===== Python3 Path =====\n"
    which python3

  printf "\n\n ===== Pip3 Version =====\n"
    pip3 -V

  printf "\n\n ===== Pip3 Path =====\n"
    which pip3
}

function brew_install (){
     /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
     (echo; echo 'eval "$(/usr/local/bin/brew shellenv)"') | tee "${HOME}/.zprofile" "${HOME}/.zshrc" "${HOME}.bashrc"
     eval "$(/usr/local/bin/brew shellenv)"
     source "${HOME}/.zprofile"; source "${HOME}/.zprofile";
     brew install openssl readline sqlite3 xz zlib pyenv
}

main(){
  TIME_STAMP=$(date +%s)
  (
   printf "\n\n ===== System Info =====\n"
   uname -ap
   printf "\n\n ====== Pre Brew === \n"
   get_python_info
   get_python3_info
  ) >> "${HOME}/Downloads/00-mac-info-${USER}-${HOST}-${TIME_STAMP}.txt"

  brew_install
  (
   printf "\n\n ====== Post Brew === \n"
   get_python_info
   get_python3_info
  ) >> "${HOME}/Downloads/00-mac-info-${USER}-${HOST}-${TIME_STAMP}.txt"
}

main
